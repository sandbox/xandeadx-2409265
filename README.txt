Installation:
-------------

1. Download QueryPath library from https://github.com/technosophos/querypath/tags

2. Extract archive in sites/all/libraries/querypath. File structure after extract must be:
  sites/all/libraries/querypath/src/qp.php
  sites/all/libraries/querypath/README.md
  ...

3. Install module.
